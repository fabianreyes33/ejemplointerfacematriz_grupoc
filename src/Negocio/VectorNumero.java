/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Interface.IMatriz;

/**
 *
 * @author madar
 */
public class VectorNumero implements IMatriz{
    
    int numeros[];

    public VectorNumero() {
    }
    
    
    public VectorNumero(int n) throws Exception {
        if(n<=0)
            throw new Exception("No se puede crear el vector:");
        this.numeros=new int[n];
        
        this.crearNumeros(n);
        
    }
    
    private void crearNumeros(int n)
    {
        
     for(int i=0;i<n;i++)   
     {
         this.numeros[i]=i;
     }
    
    }

    public int[] getNumeros() {
        return numeros;
    }

    public void setNumeros(int[] numeros) {
        this.numeros = numeros;
    }

    @Override
    public String toString() {
        
        if(this.numeros==null)
            return ("Vector vacío");
        String msg="";
        for(int dato:this.numeros)
            msg+=dato+"\t";
        
        return msg;
        
    }

    @Override
    public int getSumaTotal() {
        int t=0;
        for(int dato:this.numeros)
           t+=dato;
        
        return t;
    }
    
    
    
}
